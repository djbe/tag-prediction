
// Todo class
Todos.Todo = Ember.Object.extend({
	'title': null,
	'isDone': false,
	'tags': []
});

// Todos list
Todos.todosController = Ember.ArrayProxy.create({
	'content': [],
	
	createTodo: function(title) {
		// get all the tags for this todo
		var extracted = Todos.tagsController.extractTags(title);
		var tags = Todos.tagsController.get('added').copy();
		
		// merge both extracted and added
		$.each(extracted, function(i, tag) {
			found = _findTag(tags, tag.name);
			if (found)
				found.set('confidence', 100);
			else
				tags.push(tag);
		});
		
		// clear tags controller
		Todos.tagsController.clear();
		
		// make them unremovable
		tags.setEach('removable', false);
		
		// create todo
		var todo = Todos.Todo.create({
			'title': Todos.tagsController.removeTags(title),
			'tags': tags.sort(_compareTags)
		});
		
		this.pushObject(todo);
	},
	
	clearCompletedTodos: function() {
		this.filterProperty('isDone', true).forEach(this.removeObject, this);
	},
	
	remaining: function() {
		return this.filterProperty('isDone', false).get('length');
	}.property('@each.isDone').cacheable(),
	
	allAreDone: function(key, value) {
		if (value !== undefined) {
			this.setEach('isDone', value);
			
			return value;
		} else {
			return !!this.get('length') && this.everyProperty('isDone', true);
		}
	}.property('@each.isDone')
});

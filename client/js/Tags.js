
Todos.Tag = Ember.Object.extend({
	'name': null,
	'confidence': 0,
	'added': false,
	'removable': true,
	
	watchAdded: function() {
		self = this;
		if (this.added)
			return;
		
		if (!_findTag(Todos.tagsController.get('suggested'), this.name))
			Todos.tagsController.deleteTag(this.name);
	}.observes('added')
});

Todos.tagsController = Ember.ArrayProxy.create({
	'content': [],
	'suggested': [],
	
	// load tags from a todo string
	// TODO: should be done from server result
	loadTags: function(data) {
		console.log(data);
		self = this;
		tags = this._arrayToTags(data);
		this.set('suggested', tags.copy());
		
		// merge with already added
		$.each(this.get('added'), function(i, tag) {
			found = _findTag(tags, tag.name);
			if (found)
				found.set('added', true);
			else
				tags.push(tag);
		});
		
		// store
		this.clear();
		this.pushObjects(tags);
	},
	
	// get subset of added tags
	added: function() {
		return this.filterProperty('added', true);
	}.property('@each.added').cacheable(),
	
	// get subset of unadded tags
	notadded: function() {
		return this.filterProperty('added', false);
	}.property('@each.added').cacheable(),
	
	// extract tags from a todo string
	extractTags: function(todo) {
		// extract tags from todo
		tags = todo.w().filter(function(val) { return _isTag(val); });
		
		// create tag objects
		return tags.map(function (item, index, self) {
			return Todos.Tag.create({
				name: item.substr(1),
				added: true,
				confidence: 100
			});
		}).sort(_compareTags);
	},
	
	// remove tags from a todo string
	removeTags: function(todo) {
		return todo.w().filter(function(val) { return !_isTag(val); }).join(' ');
	},
	
	// clear tags list
	clear: function() {
		this.set('content', []);
	},
	
	deleteTag: function(name) {
		// find tag index
		index = -1;
		$.each(this.get('content'), function(i, item) {
			if (item.get('name') == name)
				index = i;
		});
		
		if (index >= 0)
			this.replaceContent(index, 1, null);
	},
	
	_arrayToTags: function(data) {
		if (typeof data != "object")
			return [];
		
		return data.map(function(item) {
			return Todos.Tag.create({
				name: item.tag.substr(1),
				added: false,
				confidence: item.confidence
			});
		}).sort(_compareTags);
	}
});

function _isTag(word) {
	return word.charAt(0) === '#';
}

function _findTag(list, name) {
	found = $.grep(list, function(e) { return e.name == name; });
	
	return (found.length > 0) ? found[0] : null;
}

function _compareTags(a, b) {
	if (a.confidence == b.confidence) {
		if (a.name < b.name)
		 	return -1;
		if (a.name > b.name)
			return 1;
		return 0;
	}
	
	return b.confidence - a.confidence
}

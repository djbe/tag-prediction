
// Namespace (application)
Todos = Ember.Application.create();

// Includes
$.include("js/Todos.js");
$.include("js/Tags.js");

// --- Views ---

Todos.StatsView = Ember.View.extend({
	remainingBinding: 'Todos.todosController.remaining',
	
	remainingString: function() {
		var remaining = this.get('remaining');
		return remaining + (remaining === 1 ? " item" : " items");
	}.property('remaining')
});

// SUGGESTION: Think about using Ember.TextArea to get a bigger input field
Todos.CreateTodoView = Ember.TextField.extend({
	m_timer: null,
	
	waitFetchTags: function() {
		clearTimeout(this.m_timer);
		this.m_timer = setTimeout(this.fetchTags, 500, this);
	}.observes('value'),
	
	insertNewline: function() {
		var value = this.get('value');
		
		if (value) {
			Todos.todosController.createTodo(value);
			this.set('value', '');
		}
	},
	
	fetchTags: function(self) {
		clearTimeout(self.m_timer);
		
		// result will be passed to loadTags method in tagsController
		var request = $.ajax({
			url: 'http://localhost:8080/tag/' + encodeURIComponent(self.get('value')),
			type: 'get',
			dataType : 'json',
		    timeout : 20000,
		    tryCount : 0,
		    retryLimit : 3,
			success: function(data) {
				Todos.tagsController.loadTags(data);
			},
		    error : function(xhr, textStatus, errorThrown ) {
		        if (textStatus == 'timeout') {
		            this.tryCount++;
		            if (this.tryCount <= this.retryLimit) {
		                $.ajax(this); // try again
		                return;
		            }
		            alert('We have tried ' + this.retryLimit + ' times and it is still not working. We give in. Sorry.');
		            return;
		        }
		        if (xhr.status == 500)
		            alert('Oops! There seems to be a server problem, please try again later.');
		        else
		            alert('Oops! There was a problem getting a server response, sorry.');
		    }
		});
	}
});

Todos.TagListView = Ember.CollectionView.extend({
	render: function(buffer) {
		buffer.push('<br /><br />' + this.title);
		this._super(buffer);
	},
	
	checkContent: function() {
		this.set('isVisible', this.content && this.content.length > 0);
		this.rerender();
	}.observes('content')
});

Todos.TagView = Ember.View.extend({
	classNames: ['todos-tag'],
	
	defaultTemplate: Ember.Handlebars.compile('<span {{bindAttr title="confidenceString"}}>{{content.name}}</span>'),
	
	confidenceString: function() {
		return 'Confidence: ' + this.content.confidence + '%';
	}.property('content'),
	
	mouseUp: function(event) {
		if (this.content.removable)
			this.content.set('added', !this.content.added);
	}
});

To start the server, execute the following command:

	$ java -jar Server.jar

This must be done with as working directory the top folder of the project. The
client and data directories should be next to the Server.jar file. Once started
the server will display the following message:

	Listening on port 8080

You now can access the client using your browser and navigating to your local
address on the mentioned port. Please note that the last '/' is mandatory!

	http://localhost:8080/client/

Once in the client, start typing your todo. Once you pause typing for half a
second the client will fetch tag suggestions for it. To add any of the tags
from the suggestion list you only need to click on it. To remove a tag just 
click on it again.

Should you enter a todo already annotated with tags, then these will be parsed
and merged with any tags you added using the client interface. Tags will
display their confidence value on mouse over, with tags extracted from the todo
always getting 100% confidence.

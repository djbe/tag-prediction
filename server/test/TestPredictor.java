package test;

import graph.Graph;
import graph.Tag;
import graph.Todo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import stemmer.IStemmer;
import tagPredictor.ITagPredictor;
import util.Debug;

public abstract class TestPredictor {
	public final static String FILENAME = "data/todo.txt";

	private List<Todo> testData;
	private Graph db;
	private IStemmer stemmer;
	private ITagPredictor predictor;

	public TestResult doTest(String fileName) throws IOException {
		init();

		// System.out.println("Stemmer   = " + db.getStemmer());
		// System.out.println("Predictor = " +
		// predictor.getClass().getCanonicalName());

		splitDataSet(fileName);
		predictor = Debug.getPredictor(db);

		TestResult result = new TestResult();
		
		result.accuracy = calculateAccuracy();
		result.accuracyPH = calculateAccuracy2();

		if (Debug.intermediateResults){
			System.out.println("Accuracy  = "
					+ Math.round(100 * result.accuracy) + "%");

			System.out.println("Accuracy2 = "
					+ Math.round(100 * result.accuracyPH) + "%");
		}
		
		return result;
	}

	protected void init() {
		stemmer = Debug.stemmer;
		db = new Graph(stemmer);
		predictor = null;

		testData = new ArrayList<Todo>();
	}

	protected void splitDataSet(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));

		String textLine;
		while ((textLine = br.readLine()) != null)
			if (goesToDB(textLine))
				db.add(textLine);
			else
				testData.add(new Todo(textLine, stemmer));
	}

	public abstract boolean goesToDB(String line);

	private double calculateAccuracy() {
		int result = 0;
		for (Todo todo : testData) {
			EnumMap<Tag, Double> tags = predictor.predict(todo);
			for (Tag tag : Tag.values()) {
				boolean predict = tags.containsKey(tag);
				boolean expect = todo.getTags().contains(tag);
				if (predict == expect) {
					if (Debug.message)
						System.out.println("   CORRECT");
					result++;
				} else {
					if (Debug.message) {
						System.out.print("    WRONG  : ");
						if (predict)
							System.out.println("false positive");
						else
							System.out.println("false negative");
					}
				}
			}
		}
		return ((double) result) / (testData.size() * Tag.values().length);
	}
	
	
	private double calculateAccuracy2() {
		int result = 0;
		int total = 0;
		for (Todo todo : testData) {
			EnumMap<Tag, Double> tags = predictor.predict(todo);
			total+= tags.size();
			for (Tag tag : Tag.values()) {
				boolean predict = tags.containsKey(tag);
				boolean expect = todo.getTags().contains(tag);
				if (predict == expect) {
					if (Debug.message)
						System.out.println("   CORRECT");
					if(expect)
						result++;
				} else {
					if (Debug.message) {
						System.out.print("    WRONG  : ");
						if (predict)
							System.out.println("false positive");
						else
							System.out.println("false negative");
					}
				}
			}
		}
		return ((double) result) / total;
	}

}

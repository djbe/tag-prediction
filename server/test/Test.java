package test;

import java.io.IOException;

public class Test extends TestPredictor {

	public static void main(String[] args) throws IOException {
		//Debug.printSetup();
		
		for (int f = 2; f <= 7; f++) {
			System.out.println(100 / f + "% testdata");

			double accuracy = 0;
			double accuracy2 = 0;
			for (int i = 0; i < f; i++){
				TestResult test = new Test().doTest(FILENAME, 5, i);
				accuracy += test.accuracy;
				accuracy2 += test.accuracyPH;
			}
			accuracy /= f;
			accuracy2 /= f;
			System.out.println("Overall accuracy = " + Math.round(100*accuracy) + "%");
			System.out.println("Overall accuracy2= " + Math.round(100*accuracy2) + "%");
			System.out.println();
		}
	}

	private int count = 0;
	private int every = 5;

	public TestResult doTest(String fileName, int every, int offset)
			throws IOException {
		count = every - offset;
		this.every = every;
		return super.doTest(fileName);
	}

	
	public TestResult doTest(String fileName, int every) throws IOException {
		return doTest(fileName, every, 0);
	}

	@Override
	public boolean goesToDB(String line) {
		return ((count++) % every) != 0;
	}

}

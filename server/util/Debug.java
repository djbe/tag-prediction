package util;

import graph.Graph;
import stemmer.IStemmer;
import stemmer.RemoveStopWords;
import tagPredictor.*;
import tagPredictor.CoOccurrenceTagPredictor;

public class Debug {
	public static boolean message = false;
	public static boolean intermediateResults = false;

	public final static String SAMPLE_FILE = "data/todo.txt";

	// public static IStemmer stemmer = new CompositeStemmer(
	// new RemoveStopWords(), new SnowballStemmerFacade());

	// public static IStemmer stemmer = new SnowballStemmerFacade();
	public static IStemmer stemmer = new RemoveStopWords();
	// public static IStemmer stemmer = IStemmer.NoStem;

	public static double lowerbound = .30;

	public static ITagPredictor getPredictor(Graph db) {
		return getPredictor(db, lowerbound);
	}

	public static ITagPredictor getPredictor(Graph db, double lowerbound) {
		switch (1) {
		case 0:
			return new NaiveTagPredictor(db);
		case 1:
		default:
			return new TagOccurencePredictor(db, lowerbound);
		case 2:
			return new DocumentSimilarityPredictor(db, lowerbound,
					new NaiveTagPredictor(db));
		case 3:
			return new CoOccurrenceTagPredictor(db, lowerbound,
					new DocumentSimilarityPredictor(db, 0.0,
							new NaiveTagPredictor(db)));
		}
	}

	public static void printSetup(Graph db) {
		System.out.println("Stemmer   = " + stemmer);
		System.out.println("Predictor = " + getPredictor(db).toString());
		// + getPredictor(null).getClass().getCanonicalName());
		System.out.println();
	}
}

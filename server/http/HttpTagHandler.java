package http;

import graph.Tag;
import graph.Todo;
import org.apache.http.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONStringer;
import org.json.JSONWriter;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 17/12/11
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
public class HttpTagHandler implements HttpRequestHandler {
	private final String docRoot;

	public HttpTagHandler(final String docRoot) {
		super();
		this.docRoot = docRoot;
	}

	public void handle(
			final HttpRequest request,
			final HttpResponse response,
			final HttpContext context) throws HttpException, IOException {

		String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
		if (!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST"))
			throw new MethodNotSupportedException(method + " method not supported");
		String target = request.getRequestLine().getUri();

		if (request instanceof HttpEntityEnclosingRequest) {
			HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
			byte[] entityContent = EntityUtils.toByteArray(entity);
			System.out.println("Incoming entity content (bytes): " + entityContent.length);
		}

		// extract todo
		String path = URLDecoder.decode(target, "UTF-8");
		String todo = (path.length() < docRoot.length()) ? "" : path.substring(docRoot.length());
		path = (path.length() < docRoot.length()) ? path : path.substring(0, docRoot.length());

		// if wrong path show denied
		if (!path.equals(docRoot)) {
			response.setStatusCode(HttpStatus.SC_FORBIDDEN);
			StringEntity entity = new StringEntity(
					"<html><body><h1>Access denied</h1></body></html>",
					ContentType.create("text/html", "UTF-8"));
			response.setEntity(entity);
			System.out.println("Request '" + path + todo + "' not supported");
		} else {
			System.out.println("Tagging '" + todo + "'");

			response.setStatusCode(HttpStatus.SC_OK);
			String result = predictTags(todo);
			response.setEntity(new StringEntity(result, ContentType.create("application/json", "UTF-8")));

			System.out.println("Result :" + result);
		}
	}

	private String predictTags(String todo) {
		try {
			JSONWriter writer = new JSONStringer().array();

			for (Map.Entry<Tag, Double> entry : Server.INSTANCE.predict(todo).entrySet())
				writer.object()
						.key("tag").value(entry.getKey())
						.key("confidence").value((int) (entry.getValue() * 100))
					  .endObject();

			return writer.endArray().toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
	}
}

package http;

import org.apache.http.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 21/12/11
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
public class HttpFileHandler implements HttpRequestHandler {
	private final String docRoot;

	public HttpFileHandler(final String docRoot) {
		super();
		this.docRoot = docRoot;
	}

	public void handle(
			final HttpRequest request,
			final HttpResponse response,
			final HttpContext context) throws HttpException, IOException {

		String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
		if (!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST")) {
			throw new MethodNotSupportedException(method + " method not supported");
		}
		String target = request.getRequestLine().getUri();

		if (request instanceof HttpEntityEnclosingRequest) {
			HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
			byte[] entityContent = EntityUtils.toByteArray(entity);
			System.out.println("Incoming entity content (bytes): " + entityContent.length);
		}

		String path = URLDecoder.decode(target, "UTF-8");
		if (path.equals("/client/"))
			path += "index.html";
		final File file = new File(this.docRoot, path);

		if (!file.exists()) {
			response.setStatusCode(HttpStatus.SC_NOT_FOUND);
			StringEntity entity = new StringEntity(
					"<html><body><h1>File" + file.getPath() +
					" not found</h1></body></html>",
					ContentType.create("text/html", "UTF-8"));
			response.setEntity(entity);
			System.out.println("File " + file.getPath() + " not found");
		} else if (!file.canRead() || file.isDirectory()) {
			response.setStatusCode(HttpStatus.SC_FORBIDDEN);
			StringEntity entity = new StringEntity(
					"<html><body><h1>Access denied</h1></body></html>",
					ContentType.create("text/html", "UTF-8"));
			response.setEntity(entity);
			System.out.println("Cannot read file " + file.getPath());
		} else {
			response.setStatusCode(HttpStatus.SC_OK);
			FileEntity body = new FileEntity(file, getMimeType(file));
			response.setEntity(body);
		}
	}

	public ContentType getMimeType(File file) {
		String extension = file.getName();
		extension = extension.substring(extension.lastIndexOf('.') + 1);

		if (extension.equals("css"))
			return ContentType.create("text/css", null);
		else if (extension.equals("js"))
			return ContentType.create("application/javascript", null);
		else
			return ContentType.create("text/html", null);
	}
}

package http;

import graph.Graph;
import graph.Tag;
import graph.Todo;
import tagPredictor.*;

import java.util.EnumMap;

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 17/12/11
 * Time: 16:30
 * To change this template use File | Settings | File Templates.
 */
public enum Server {
	INSTANCE;

	private static final String docRoot = ".";
	private Graph m_db;
	private ITagPredictor m_predictor;
	private Thread m_thread;

	public static void main(String[] args) throws Exception {
		INSTANCE.start();
    }

	private Server() {
		try {
			// load data
			m_db = Graph.readFromFile("data/todo.txt");
			m_predictor = new CoOccurrenceTagPredictor(m_db, 0.20,
					new DocumentSimilarityPredictor(m_db, 0.0,
							new NaiveTagPredictor(m_db)));

			// create actual server
			m_thread = new RequestListenerThread(8080, docRoot);
			m_thread.setDaemon(false);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public void start() {
		m_thread.start();
	}

	public EnumMap<Tag, Double> predict(String todo) {
		return m_predictor.predict(new Todo(todo, m_db.getStemmer()));
	}
}

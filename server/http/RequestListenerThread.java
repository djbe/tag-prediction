package http;

import org.apache.http.HttpResponseInterceptor;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.protocol.*;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 17/12/11
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
public class RequestListenerThread extends Thread {
	private final ServerSocket serversocket;
	private final HttpParams params;
	private final HttpService httpService;

	public RequestListenerThread(int port, final String docroot) throws IOException {
		this.serversocket = new ServerSocket(port);
		this.params = new SyncBasicHttpParams();
		this.params
			.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 5000)
			.setIntParameter(CoreConnectionPNames.SOCKET_BUFFER_SIZE, 8 * 1024)
			.setBooleanParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false)
			.setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true)
			.setParameter(CoreProtocolPNames.ORIGIN_SERVER, "HttpComponents/1.1");

		// Set up the HTTP protocol processor
		HttpProcessor httpproc = new ImmutableHttpProcessor(new HttpResponseInterceptor[] {
				new ResponseDate(),
				new ResponseServer(),
				new ResponseContent(),
				new ResponseConnControl()
		});

		// Set up request handlers
		HttpRequestHandlerRegistry reqistry = new HttpRequestHandlerRegistry();
		reqistry.register("/tag/*", new HttpTagHandler("/tag/"));
		reqistry.register("/client/*", new HttpFileHandler(docroot));

		// Set up the HTTP service
		this.httpService = new HttpService(
				httpproc,
				new DefaultConnectionReuseStrategy(),
				new DefaultHttpResponseFactory(),
				reqistry,
				this.params);
	}

	@Override
	public void run() {
		System.out.println("Listening on port " + this.serversocket.getLocalPort());
		while (!Thread.interrupted()) {
			try {
				// Set up HTTP connection
				Socket socket = this.serversocket.accept();
				DefaultHttpServerConnection conn = new DefaultHttpServerConnection();
				//System.out.println("Incoming connection from " + socket.getInetAddress());
				conn.bind(socket, this.params);

				// Start worker thread
				Thread t = new WorkerThread(this.httpService, conn);
				t.setDaemon(true);
				t.start();
			} catch (InterruptedIOException ex) {
				break;
			} catch (IOException e) {
				System.err.println("I/O error initialising connection thread: "
						+ e.getMessage());
				break;
			}
		}
	}
}

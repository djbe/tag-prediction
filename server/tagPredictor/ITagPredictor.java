package tagPredictor;

import graph.Tag;
import graph.Todo;

import java.util.EnumMap;


public interface ITagPredictor {
	public EnumMap<Tag, Double> predict(Todo todo);
}

package tagPredictor;

import graph.Graph;
import graph.Tag;
import graph.Todo;

import java.util.EnumMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 21/12/11
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */
public class CoOccurrenceTagPredictor extends LowerFrequencyBoundTagPredictor {
	private ITagPredictor subPredictor;
	private EnumMap<Tag, EnumMap<Tag, Double>> factors;

	public CoOccurrenceTagPredictor(Graph db, double lowerFrequencyBound, ITagPredictor subPredictor) {
		super(db, lowerFrequencyBound);
		this.subPredictor = subPredictor;

		// calculate co-occurence factor for 2 tags
		factors = new EnumMap<Tag, EnumMap<Tag, Double>>(Tag.class);
		for (Tag t : Tag.values()) {
			EnumMap<Tag, Double> tagMap = new EnumMap<Tag, Double>(Tag.class);
			List<Todo> todos = db.getTodosContaining(t);

			for (Tag t2 : Tag.values()) {
				int counter = 0;
				for (Todo todo : todos)
					counter += todo.hasTag(t2) ? 1 : 0;
				tagMap.put(t2, counter / (double)todos.size());
			}

			factors.put(t, tagMap);
		}
	}

	public EnumMap<Tag, Double> calculateTagFrequencies(Todo todo) {
		EnumMap<Tag, Double> temp = subPredictor.predict(todo);
		EnumMap<Tag, Double> result = new EnumMap<Tag, Double>(Tag.class);

		for (Tag t : Tag.values()) {
			double sum = 0;
			double total = 0;

			for (Tag t2 : temp.keySet()) {
				sum += temp.get(t2) * factors.get(t).get(t2);
			    total += factors.get(t).get(t2);
			}

			result.put(t, sum / total);
		}

		return result;
	}
}

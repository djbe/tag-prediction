package tagPredictor;

import graph.Graph;
import graph.Tag;
import graph.Todo;

import java.util.EnumMap;
import java.util.List;

import util.Debug;

public class TagOccurencePredictor extends LowerFrequencyBoundTagPredictor {
	public TagOccurencePredictor(Graph db, double lowerFrequencyBound) {
		super(db, lowerFrequencyBound);
	}

	@Override
	public EnumMap<Tag, Double> calculateTagFrequencies(Todo todo) {
		int totalNumberOfTodos = 0;

		// init all tag frequencies to 0
		EnumMap<Tag, Integer> tagOccurrence = new EnumMap<Tag, Integer>(
				Tag.class);
		for (Tag t : Tag.values())
			tagOccurrence.put(t, 0);

		// for each word
		for (String word : todo.getWords()) {
			if (Debug.intermediateResults)
				System.out.println(word);

			List<Todo> todos = db.getTodosContaining(word);
			totalNumberOfTodos += todos.size();

			// get all linked todos
			for (Todo t : todos) {
				if (Debug.intermediateResults)
					System.out.println(" - " + t);
				// for all their tags: increment the corresponding tag
				// occurrence
				for (Tag tag : t.getTags())
					tagOccurrence.put(tag, tagOccurrence.get(tag) + 1);
			}
		}

		// calculate frequencies
		EnumMap<Tag, Double> result = new EnumMap<Tag, Double>(Tag.class);
		for (Tag t : tagOccurrence.keySet()) {
			// System.out.println(tagOccurrence.get(t) + "\t" +
			// totalNumberOfTodos + "\t" + ((double)tagOccurrence.get(t)) /
			// totalNumberOfTodos);
			result.put(t, ((double) tagOccurrence.get(t)) / totalNumberOfTodos);
		}
		return result;
	}
}

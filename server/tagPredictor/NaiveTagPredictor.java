package tagPredictor;

import graph.Graph;
import graph.Tag;

import java.util.EnumMap;

/**
 * Will predict all tags that can be reached from the given graph
 */
public class NaiveTagPredictor extends TagOccurencePredictor{
	public NaiveTagPredictor(Graph db) {
		super(db, 0);
	}
}

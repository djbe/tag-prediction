package tagPredictor;

import graph.Graph;
import graph.Tag;
import graph.Todo;
import util.Debug;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 21/12/11
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */
public class DocumentSimilarityPredictor extends LowerFrequencyBoundTagPredictor {
	private Map<Todo, EnumMap<Tag, Double>> subPredictions;

	public DocumentSimilarityPredictor(Graph db, double lowerFrequencyBound, ITagPredictor subPredictor) {
		super(db, lowerFrequencyBound);

		// pre-calculate tag predictions for all todos
		subPredictions = new HashMap<Todo, EnumMap<Tag, Double>>();
		for (Todo todo : db.getTodos()) {
			if (Debug.message)
				System.out.println("Precalculating for: " + todo.getText());
			subPredictions.put(todo, subPredictor.predict(todo));
		}
	}

	public EnumMap<Tag, Double> calculateTagFrequencies(Todo todo) {
		double divider = 0;

		// init frequencies
		EnumMap<Tag, Double> result = new EnumMap<Tag, Double>(Tag.class);
		for (Tag t : Tag.values())
			result.put(t, 0.0);

		// for each word
		for (String word : todo.getWords()) {
			List<Todo> todos = db.getTodosContaining(word);

			for (Todo t : todos) {
				// calculate similarity
				double similarity = todo.similarity(t);
				if (Debug.message) {
					System.out.println("todo: " + t);
					System.out.println("similarity: " + similarity);
				}

				// combine with tag prediction
				EnumMap<Tag, Double> predictions = subPredictions.get(t);
				for (Tag tag : predictions.keySet())
					result.put(tag, result.get(tag) + similarity * predictions.get(tag));

				divider += similarity;
			}
		}

		// average frequencies
		for (Tag t : result.keySet())
			result.put(t, result.get(t) / divider);

		return result;
	}
}

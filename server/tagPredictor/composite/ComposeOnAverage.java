package tagPredictor.composite;

import tagPredictor.ITagPredictor;

public class ComposeOnAverage extends AbstractCompositePredictor{

	public ComposeOnAverage(ITagPredictor predictor1, ITagPredictor predictor2) {
		super(predictor1, predictor2);
	}

	@Override
	protected Double combine(Double val1, Double val2) {
		return (val1+val2)/2;
	}

}

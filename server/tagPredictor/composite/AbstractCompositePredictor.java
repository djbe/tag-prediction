package tagPredictor.composite;

import graph.Tag;

import java.util.EnumMap;

import graph.Todo;
import tagPredictor.ITagPredictor;

public abstract class AbstractCompositePredictor implements ITagPredictor{

	
	private ITagPredictor predictor1;
	private ITagPredictor predictor2;

	public AbstractCompositePredictor(ITagPredictor predictor1, ITagPredictor predictor2){
		this.predictor1 = predictor1;
		this.predictor2 = predictor2;
	}

	@Override
	public EnumMap<Tag, Double> predict(Todo sentence) {
		EnumMap<Tag, Double> result1 = predictor1.predict(sentence);
		EnumMap<Tag, Double> result2 = predictor2.predict(sentence);
		
		EnumMap<Tag, Double> result = new EnumMap<Tag, Double>(Tag.class);
		for(Tag t : Tag.values()){
			ensureKey(result1, t);
			ensureKey(result2, t);
			result.put(t,combine(result1.get(t), result2.get(t)));
		}
		return result;
	}

	protected abstract Double combine(Double val1, Double val2);

	private void ensureKey(EnumMap<Tag, Double> map, Tag t) {
		if(!map.containsKey(t))
			map.put(t,0.0);
	}
	
	
	
}

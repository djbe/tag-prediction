package tagPredictor;

import graph.Graph;
import graph.Tag;
import graph.Todo;

import java.util.EnumMap;

/**
 * Will predict all tags that can be reached from the given graph
 */
public abstract class LowerFrequencyBoundTagPredictor implements ITagPredictor {
	private double lowerFrequencyBound;
	protected Graph db;

	public LowerFrequencyBoundTagPredictor(Graph db, double lowerFrequencyBound) {
		this.db = db;
		this.lowerFrequencyBound = lowerFrequencyBound;
	}

	public EnumMap<Tag, Double> predict(Todo todo) {
		EnumMap<Tag, Double> frequencies = calculateTagFrequencies(todo);
		EnumMap<Tag, Double> result = new EnumMap<Tag, Double>(Tag.class);

		for (Tag t : frequencies.keySet())
			if (frequencies.get(t) >= lowerFrequencyBound)
				result.put(t, frequencies.get(t));

		return result;
	}

	public abstract EnumMap<Tag, Double> calculateTagFrequencies(Todo todo);
}

package stemmer;

public class CompositeStemmer implements IStemmer{

	private IStemmer stemmer1;
	private IStemmer stemmer2;

	public CompositeStemmer(IStemmer stemmer1, IStemmer stemmer2){
		this.stemmer1 = stemmer1;
		this.stemmer2 = stemmer2;
	}
	@Override
	public String stem(String input) {
		return stemmer2.stem(stemmer1.stem(input));
	}

	@Override
	public String toString() {
		return stemmer1 + " and " + stemmer2;
	}
}

package stemmer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * removes stopwords and punctuation
 * converts to lowercase
 */
public class RemoveStopWords implements IStemmer {

	public final static String stopwords = getStopwords();

	@Override
	public String stem(String input) {
		StringBuilder result = new StringBuilder();
		for(String word : input.split("\\s+")){
			word = word.toLowerCase().replaceAll("[^\\w\\s']", "");
			if(!word.matches(stopwords)){
				result.append(word);
				result.append(" ");
			}
		}
		result.deleteCharAt(result.length()-1);
		return result.toString();
	}

	@Override
	public String toString() {
		return "Remove_StopWords";
	}
	
	
	private final static String getStopwords(){
		 try {
			StringBuilder result = new StringBuilder();
			BufferedReader in = new BufferedReader(new FileReader("data/stopwords.txt"));
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
				result.append("|");
			}
			result.deleteCharAt(result.length()-1);
			return result.toString();
		} catch (IOException e) {
			e.printStackTrace();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
			}
			
			return "a|an|and|the|of";
		}
	}
	
	

}

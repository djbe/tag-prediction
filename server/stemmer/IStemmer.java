package stemmer;

public interface IStemmer {

	public String stem(String input);
	
	
	public final static IStemmer NoStem = new IStemmer() {
		@Override
		public String stem(String input) {
			return input;
		}
	};
}

package stemmer;

import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.englishStemmer;

public class SnowballStemmerFacade implements IStemmer {
	protected SnowballStemmer stemmer;
	
	public SnowballStemmerFacade(SnowballStemmer stemmer){
		this.stemmer = stemmer;
	}
	
	public SnowballStemmerFacade(){
		this(new englishStemmer());
	}
	
	@Override
	public String stem(String input) {
		StringBuilder result = new StringBuilder(input.length());
		for(String word : input.split("\\s+")){
			stemmer.setCurrent(word);
			stemmer.stem();
			result.append(" ");
			result.append(stemmer.getCurrent());
		}
		result.deleteCharAt(0);
		return result.toString();
	}
	
	@Override
	public String toString(){
		return stemmer.getClass().getSimpleName();
	}

}

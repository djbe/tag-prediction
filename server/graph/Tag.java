package graph;

public enum Tag {
	appointment, chore, family, finance, friends, home, leisure, mlcourse, reading, school, shop, sport, travel, urgent, work;

	@Override
	public String toString() {
		return "#" + super.toString();
	}

	
	/**
	 * make sure the enum is 
	 */
	@SuppressWarnings("unused")
	private static void sortAlphabetically() {
		Tag[] x = Tag.values().clone();
		int j;
		boolean flag = true; // will determine when the sort is finished
		Tag temp;

		while (flag) {
			flag = false;
			for (j = 0; j < x.length - 1; j++) {
				if (x[j].toString().compareToIgnoreCase(x[j + 1].toString()) > 0) { // ascending sort
					temp = x[j];
					x[j] = x[j + 1]; // swapping
					x[j + 1] = temp;
					flag = true;
				}
			}
		}

		for (Tag t : x)
			System.out.println(t + ",");
	}
}

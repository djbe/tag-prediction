package graph;

import java.util.*;

import stemmer.IStemmer;


/**
 * Represents a line of text, accompanied by some tags
 */
public class Todo {
	private String text;
	private List<String> words;
	private EnumSet<Tag> tags;

	private Todo(String text, List<String> stemmed){
		this.text = text;
		this.words = stemmed;
		this.tags = EnumSet.noneOf(Tag.class);
	}
	
	public Todo(String todo) {
		this(todo, IStemmer.NoStem);
	}

	public Todo(String todo, IStemmer stemmer) {
		tags = EnumSet.noneOf(Tag.class);
		text = extractTags(todo, tags);
		words = extractWords(text, stemmer);
	}
	
	public Todo withoutTags(){
		return new Todo(this.text, this.words);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(text);
		for (Tag t : tags) {
			sb.append(" ");
			sb.append(t.toString());
		}
		return sb.toString();
	}

	public String getText() {
		return text;
	}

	public List<String> getWords() {
		return words;
	}

	public Set<Tag> getTags() {
		return Collections.unmodifiableSet(tags);
	}

	public double similarity(Todo todo) {
		// get all words of both
		List<String> all = new ArrayList<String>(words);
		all.addAll(todo.words);

		// TODO: check if linked

		// sum of words present in both todos
		double sum = 0.0;
		for (String word : all)
			sum += words.contains(word) && todo.words.contains(word) ? 1.0 : 0.0;

		return sum / all.size();
	}

	/**
	 * adds all the tags in todo to the given enumset and removes them from the
	 * string
	 *
	 * @param todo
	 *            string containing arbitrary text and tags
	 * @param tags
	 *            enumset to add all tags to
	 * @return the string without the tags
	 */
	private static String extractTags(String todo, EnumSet<Tag> tags) {
		String temp;
		for (Tag t : Tag.values()) {
			temp = todo.replaceAll("\\s*" + t.toString(), "");
			if (temp.length() != todo.length()) {
				tags.add(t);
				todo = temp;
			}
		}

		return todo;
	}

	/**
	 * Gets all the unique lemmatized words from a todo
	 */
	private static List<String> extractWords(String todo, IStemmer stemmer) {
		// get words
		List<String> words = Arrays.asList(stemmer.stem(todo).split("\\W+"));

		// only unique words
		return new ArrayList<String>(new HashSet<String>(words));
	}

	public boolean hasTag(Tag tag) {
		return tags.contains(tag);
	}
}

package graph;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import stemmer.IStemmer;
import stemmer.SnowballStemmerFacade;
import tagPredictor.TagOccurencePredictor;
import tagPredictor.ITagPredictor;
import util.Debug;

public class Graph {

	private List<Todo> todos;
	private Map<String, List<Todo>> graph;
	private Map<Tag, List<Todo>> todosPerTag;

	private IStemmer stemmer;

	/**
	 * builds the graph from the default todo file "data/todo.txt" and requests
	 * user entered sentences to predict tags.
	 * 
	 * @param args
	 *            (ignored)
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Graph db = readFromFile("data/todo.txt");
		// ITagPredictor predictor = new NaiveTagPredictor(db);
		ITagPredictor predictor = new TagOccurencePredictor(db, .20);

		if (Debug.message) {
			System.out.println("\n");
			Debug.printSetup(db);
		}
		System.out.println("Enter sentence to predict tags for it:");
		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println(predictor.predict(new Todo(scan.nextLine(), db
					.getStemmer())));
		}
	}

	public Graph() {
		this(new SnowballStemmerFacade());
	}

	public Graph(IStemmer stemmer) {
		this.stemmer = stemmer;
		this.todos = new ArrayList<Todo>();
		this.graph = new HashMap<String, List<Todo>>();
		this.todosPerTag = new HashMap<Tag, List<Todo>>();

		for (Tag tag : Tag.values())
			todosPerTag.put(tag, new ArrayList<Todo>());
	}

	/**
	 * Builds the graph from the given file. The file should contain the one
	 * todo per line. Tags should be included with every todo.
	 * 
	 * @param fileName
	 *            path to the file
	 * @return the built graph
	 * @throws IOException
	 */
	public static Graph readFromFile(String fileName) throws IOException {
		return readFromFile(fileName, Debug.stemmer);
	}

	public static Graph readFromFile(String fileName, IStemmer stemmer)
			throws IOException {
		Graph db = new Graph(stemmer);

		BufferedReader br = new BufferedReader(new FileReader(fileName));

		String textLine;
		while ((textLine = br.readLine()) != null)
			db.add(textLine);

		return db;
	}

	/**
	 * adds a todo to the graph
	 */
	public void add(String todo) {
		if (todo.length() == 0)
			return;
		add(new Todo(todo, stemmer));

	}

	/**
	 * adds a todo to the graph.
	 * 
	 * @param todo
	 *            (should use the same stemmer as the database)
	 */
	private void add(Todo todo) {
		// TODO? test if todo uses the same stemmer as database

		todos.add(todo);

		// for each word in this todo: add a reference to this todo to the graph
		for (String word : todo.getWords()) {
			if (!graph.containsKey(word))
				graph.put(word, new ArrayList<Todo>());
			graph.get(word).add(todo);
		}
		for (Tag tag : todo.getTags()) {
			todosPerTag.get(tag).add(todo);
		}

		if (Debug.message) {
			System.out.println(todo.getText());
			System.out.println(todo.getWords());
			System.out.println(todo.getTags());
			System.out.println();
		}
	}

	/**
	 * return a list of all todos containig the given word
	 */
	public List<Todo> getTodosContaining(String word) {
		List<Todo> result = graph.get(word);
		if (result == null)
			result = Collections.emptyList();
		return Collections.unmodifiableList(result);
	}

	public List<Todo> getTodosContaining(Tag tag) {
		return Collections.unmodifiableList(todosPerTag.get(tag));
	}

	/**
	 * gets the stemmer used when building this graph
	 */
	public IStemmer getStemmer() {
		return stemmer;
	}

	public List<Todo> getTodos() {
		return Collections.unmodifiableList(todos);
	}

}
